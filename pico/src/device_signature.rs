//! Blatently stolen from https://github.com/korken89/pico-probe/blob/master/src/device_signature.rs

use rp2040_hal::pac::{IO_QSPI, XIP_SSI};

#[inline(always)]
#[link_section = ".data.ram_func"]
unsafe fn set_cs(level: bool) {
    (*IO_QSPI::ptr()).gpio_qspiss.gpio_ctrl.modify(|_, w| {
        if level {
            w.outover().high()
        } else {
            w.outover().low()
        }
    });
}

#[link_section = ".data.ram_func"]
#[inline(never)]
unsafe fn do_flash_cmd(txrxbuf: &mut [u8]) {
    // Load important addresses to the stack
    let connect_internal_flash = rp2040_hal::rom_data::connect_internal_flash;
    let flash_exit_xip = rp2040_hal::rom_data::flash_exit_xip;
    let flash_flush_cache = rp2040_hal::rom_data::flash_flush_cache;

    let mut boot2: core::mem::MaybeUninit<[u8; 256]> = core::mem::MaybeUninit::uninit();

    let xip_base = 0x10000000 as *const u32;
    rp2040_hal::rom_data::memcpy(boot2.as_mut_ptr() as _, xip_base as _, 256);

    core::sync::atomic::compiler_fence(core::sync::atomic::Ordering::SeqCst);

    connect_internal_flash();
    flash_exit_xip();

    set_cs(false);

    let ssi = &*XIP_SSI::ptr();

    for b in txrxbuf {
        while !ssi.sr.read().tfnf().bit_is_set() {}
        ssi.dr0.write(|w| w.dr().bits(*b as _));

        while !ssi.sr.read().rfne().bit_is_set() {}
        *b = ssi.dr0.read().dr().bits() as _;
    }

    set_cs(true);

    flash_flush_cache();

    let ptr = (boot2.as_mut_ptr() as *const u8).add(1) as *const ();
    let start: extern "C" fn() = core::mem::transmute(ptr);
    start();
}

pub fn read_uid() -> [u8; 8] {
    const FLASH_RUID_CMD: u8 = 0x4b;
    const FLASH_RUID_DUMMY_BYTES: usize = 4;
    const FLASH_RUID_DATA_BYTES: usize = 8;
    const FLASH_RUID_TOTAL_BYTES: usize = 1 + FLASH_RUID_DUMMY_BYTES + FLASH_RUID_DATA_BYTES;

    let mut buf = [0; FLASH_RUID_TOTAL_BYTES];
    buf[0] = FLASH_RUID_CMD;

    unsafe {
        do_flash_cmd(&mut buf);
    }

    buf[FLASH_RUID_DUMMY_BYTES + 1..].try_into().unwrap()
}
