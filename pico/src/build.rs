use std::{fs::File, io::Read, path::Path, process::Command};

fn git_info() -> Option<String> {
    let mut git_string: Option<String> = None;

    if let Ok(output) = Command::new("git").args(["rev-parse", "HEAD"]).output() {
        if output.status.success() {
            let git_hash = String::from_utf8_lossy(&output.stdout);
            git_string = Some(git_hash.to_string());
        }
    }

    if let Ok(output) = Command::new("git")
        .args(["rev-parse", "--abbrev-ref", "HEAD"])
        .output()
    {
        if output.status.success() {
            let git_branch = String::from_utf8_lossy(&output.stdout);
            if let Some(ref mut hash) = &mut git_string {
                hash.push('-');
                hash.push_str(git_branch.as_ref());
            }
        }
    }

    git_string
}

fn version_info() -> Result<String, anyhow::Error> {
    let cargo_toml = Path::new(&std::env::var("CARGO_MANIFEST_DIR")?).join("Cargo.toml");

    let mut file = File::open(&cargo_toml)?;

    let mut cargo_data = String::new();
    file.read_to_string(&mut cargo_data)?;

    let data: toml::Value = toml::from_str(&cargo_data)?;

    let name = data["package"]["name"]
        .as_str()
        .ok_or_else(|| anyhow::anyhow!("Missing package name string"))?;
    let version = data["package"]["version"]
        .as_str()
        .ok_or_else(|| anyhow::anyhow!("Missing version string"))?;

    Ok(format!("{}-{}", name, version))
}

fn main() -> Result<(), anyhow::Error> {
    let version = version_info()?;
    let version = if let Some(git) = git_info() {
        format!("{}-{}", version, git)
    } else {
        version
    };

    println!("cargo:rustc-env=FIRMWARE_VERSION={}", version);

    Ok(())
}
