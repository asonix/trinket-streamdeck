use common::{ButtonPins, ButtonState};
use core::convert::Infallible;
use embedded_hal::digital::v2::InputPin;
use rp2040_hal::gpio::pin::{
    bank0::{Gpio0, Gpio14, Gpio16, Gpio2, Gpio21, Gpio22, Gpio27, Gpio4, Gpio6, Gpio8},
    Disabled, Input, Pin, PullDown, PullUp,
};

pub(crate) struct Buttons {
    pub(crate) d0: Pin<Gpio0, Disabled<PullDown>>,
    pub(crate) d1: Pin<Gpio2, Disabled<PullDown>>,
    pub(crate) d2: Pin<Gpio4, Disabled<PullDown>>,
    pub(crate) d3: Pin<Gpio6, Disabled<PullDown>>,
    pub(crate) d4: Pin<Gpio8, Disabled<PullDown>>,
    pub(crate) d5: Pin<Gpio14, Disabled<PullDown>>,
    pub(crate) d6: Pin<Gpio16, Disabled<PullDown>>,
    pub(crate) d7: Pin<Gpio21, Disabled<PullDown>>,
    pub(crate) d8: Pin<Gpio22, Disabled<PullDown>>,
    pub(crate) d9: Pin<Gpio27, Disabled<PullDown>>,
}

pub(crate) struct Pins {
    pub(crate) d0: Pin<Gpio0, Input<PullUp>>,
    pub(crate) d1: Pin<Gpio2, Input<PullUp>>,
    pub(crate) d2: Pin<Gpio4, Input<PullUp>>,
    pub(crate) d3: Pin<Gpio6, Input<PullUp>>,
    pub(crate) d4: Pin<Gpio8, Input<PullUp>>,
    pub(crate) d5: Pin<Gpio14, Input<PullUp>>,
    pub(crate) d6: Pin<Gpio16, Input<PullUp>>,
    pub(crate) d7: Pin<Gpio21, Input<PullUp>>,
    pub(crate) d8: Pin<Gpio22, Input<PullUp>>,
    pub(crate) d9: Pin<Gpio27, Input<PullUp>>,
}

impl<'a> ButtonPins<'a, 10> for Pins {
    fn to_array(&'a self) -> [&'a dyn InputPin<Error = Infallible>; 10] {
        [
            &self.d0, &self.d1, &self.d2, &self.d3, &self.d4, &self.d5, &self.d6, &self.d7,
            &self.d8, &self.d9,
        ]
    }
}

impl Buttons {
    pub(crate) fn init(self) -> ButtonState<Pins, 10> {
        ButtonState::from_pins(Pins {
            d0: self.d0.into_mode(),
            d1: self.d1.into_mode(),
            d2: self.d2.into_mode(),
            d3: self.d3.into_mode(),
            d4: self.d4.into_mode(),
            d5: self.d5.into_mode(),
            d6: self.d6.into_mode(),
            d7: self.d7.into_mode(),
            d8: self.d8.into_mode(),
            d9: self.d9.into_mode(),
        })
    }
}
