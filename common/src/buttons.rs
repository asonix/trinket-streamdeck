use core::convert::Infallible;
use embedded_hal::digital::v2::InputPin;

const DEBOUNCE_TIMEOUT: u32 = 100;

pub trait ButtonPins<'a, const N: usize> {
    fn to_array(&'a self) -> [&'a dyn InputPin<Error = Infallible>; N];
}

pub struct ButtonState<T, const N: usize> {
    pins: T,
    debounce_times: [u32; N],
    last_states: [bool; N],
    current_states: [bool; N],
}

impl<T, const N: usize> ButtonState<T, N>
where
    for<'a> T: ButtonPins<'a, N> + 'static,
{
    pub fn from_pins(pins: T) -> Self {
        ButtonState {
            pins,
            debounce_times: [0u32; N],
            last_states: [false; N],
            current_states: [false; N],
        }
    }

    pub fn tick(&mut self, current_time: u32, output: &mut [u8]) -> Option<usize> {
        if self.check_buttons(current_time)? {
            let size = self.calculate_press_state(output)?;
            self.reset_state(current_time);

            return Some(size);
        }

        None
    }

    fn check_buttons(&mut self, current_time: u32) -> Option<bool> {
        let array = self.pins.to_array();

        let iter = array
            .iter()
            .zip(&mut self.debounce_times)
            .zip(&mut self.last_states)
            .zip(&mut self.current_states);

        let mut needs_update = false;

        for (((input, debounce_time), last_state), current_state) in iter {
            // handle wrapping
            if current_time < *debounce_time {
                *debounce_time = 0;
            }

            let reading = input.is_high().ok()?;

            if reading != *last_state {
                *last_state = reading;
                *debounce_time = current_time;
            }

            if *debounce_time + DEBOUNCE_TIMEOUT < current_time && *current_state != *last_state {
                *current_state = *last_state;

                if *current_state {
                    needs_update |= true;
                }
            }
        }

        Some(needs_update)
    }

    fn reset_state(&mut self, current_time: u32) {
        for time in &mut self.debounce_times {
            *time = current_time;
        }
    }

    fn calculate_press_state(&self, output: &mut [u8]) -> Option<usize> {
        let iter = self
            .current_states
            .iter()
            .enumerate()
            .filter_map(|(i, state)| if *state { Some(i as u8) } else { None });

        let mut index = 1;
        for key in iter {
            *output.get_mut(index)? = key;
            index += 1;
        }
        *output.get_mut(0)? = (index - 1) as u8;
        Some(index)
    }
}
