const HANDSHAKE_TIMEOUT: u32 = 100;

pub struct Handshake {
    step_1_complete: bool,
    step_2_complete: bool,
    buffer: [u8; 16],
    timestamp: u32,
}

pub enum HandshakeResponse<'a> {
    Finished,
    Write { bytes: &'a [u8] },
}

impl Handshake {
    pub const fn new() -> Self {
        Handshake {
            step_1_complete: false,
            step_2_complete: false,
            buffer: [0; 16],
            timestamp: 0,
        }
    }

    pub fn is_complete(&self) -> bool {
        self.step_1_complete && self.step_2_complete
    }

    pub fn perform(
        &mut self,
        input: &[u8; 32],
        current_time: u32,
    ) -> Option<HandshakeResponse<'_>> {
        // handle wrapping
        if current_time < self.timestamp {
            self.timestamp = 0;
        }

        if self.is_complete() {
            self.step_1_complete = false;
            self.step_2_complete = false;
        }

        if !self.step_1_complete {
            self.timestamp = current_time;
            self.step_1(input)?;
            self.step_1_complete = true;
            return Some(HandshakeResponse::Write {
                bytes: &self.buffer[..],
            });
        } else if !self.step_2_complete {
            self.step_2_complete = self.step_2(input);
            if !self.step_2_complete {
                self.step_1_complete = false;
                return None;
            }
        }

        if self.step_1_complete
            && !self.step_2_complete
            && current_time > self.timestamp + HANDSHAKE_TIMEOUT
        {
            self.step_1_complete = false;
            return None;
        }

        Some(HandshakeResponse::Finished)
    }

    fn step_1(&mut self, input: &[u8; 32]) -> Option<()> {
        for ((c1, c2), out) in input[..16].iter().zip(&input[16..]).zip(&mut self.buffer) {
            *out = c1 ^ c2;
        }

        Some(())
    }

    fn step_2(&self, input: &[u8; 32]) -> bool {
        let mut succeeded = true;

        for ((c1, c2), c3) in input[..16].iter().zip(&input[16..]).zip(&self.buffer) {
            succeeded = succeeded || c1 == c2 && c1 == c3;
        }

        succeeded
    }
}
