use crate::device::Device;

static NAME: &[u8] = b"Streamdeck";
static AUTHOR: &[u8] = b"asonix <asonix@asonix.dog>";
static REPO: &[u8] = b"https://git.asonix.dog/asonix/trinket-streamdeck";

pub fn handle_input<D, const SN_LEN: usize, const EXTRAS_LEN: usize>(
    device: &mut D,
    input: &[u8; 32],
    output: &mut [u8],
    count: usize,
) -> Option<usize>
where
    D: Device<SN_LEN, EXTRAS_LEN>,
{
    if matches(input, count, b"ident") {
        let sn = device.serial_number();
        write_length_delim(&sn, output)
    } else if matches(input, count, b"name") {
        write_length_delim(NAME, output)
    } else if matches(input, count, b"repo") {
        write_length_delim(REPO, output)
    } else if matches(input, count, b"author") {
        write_length_delim(AUTHOR, output)
    } else if matches(input, count, b"reset") {
        device.reset();
        Some(0)
    } else {
        for (key, value) in D::extras() {
            if matches(input, count, key.as_bytes()) {
                return write_length_delim(value.as_bytes(), output);
            }
        }
        write_length_delim(&input[..count], output)
    }
}

fn matches(input: &[u8], count: usize, bytes: &'static [u8]) -> bool {
    count == bytes.len() && input.starts_with(bytes)
}

fn write_length_delim(bytes: &[u8], output: &mut [u8]) -> Option<usize> {
    let out_len = bytes.len() + 1;
    if output.len() < out_len {
        return None;
    }
    output[0] = bytes.len() as u8;
    output[1..out_len].copy_from_slice(bytes);

    Some(out_len)
}
