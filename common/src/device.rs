pub trait Device<const SN_LEN: usize, const EXTRAS_LEN: usize> {
    fn serial_number(&mut self) -> [u8; SN_LEN];
    fn extras() -> [(&'static str, &'static str); EXTRAS_LEN];
    fn reset(&mut self);
}
