pub struct UnsafeSync<T>(T);

impl<T> UnsafeSync<T> {
    pub const fn new(item: T) -> Self {
        UnsafeSync(item)
    }

    /// # Safety
    ///
    /// uhh...
    pub unsafe fn get(&self) -> &T {
        &self.0
    }
}

unsafe impl<T> Sync for UnsafeSync<T> where T: Send {}
