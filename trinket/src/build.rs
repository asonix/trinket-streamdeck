use std::{
    fs::File,
    io::{Read, Write},
    path::{Path, PathBuf},
    process::Command,
};

fn memory_x() {
    // Put `memory.x` in our output directory and ensure it's
    // on the linker search path.
    let out = &PathBuf::from(std::env::var_os("OUT_DIR").unwrap());
    File::create(out.join("memory.x"))
        .unwrap()
        .write_all(include_bytes!("../memory.x"))
        .unwrap();
    println!("cargo:rustc-link-search={}", out.display());

    // By default, Cargo will re-run a build script whenever
    // any file in the project changes. By specifying `memory.x`
    // here, we ensure the build script is only re-run when
    // `memory.x` is changed.
    println!("cargo:rerun-if-changed=memory.x");
}

fn git_info() -> Option<String> {
    let mut git_string: Option<String> = None;

    if let Ok(output) = Command::new("git").args(["rev-parse", "HEAD"]).output() {
        if output.status.success() {
            let git_hash = String::from_utf8_lossy(&output.stdout);
            git_string = Some(git_hash.to_string());
        }
    }

    if let Ok(output) = Command::new("git")
        .args(["rev-parse", "--abbrev-ref", "HEAD"])
        .output()
    {
        if output.status.success() {
            let git_branch = String::from_utf8_lossy(&output.stdout);
            if let Some(ref mut hash) = &mut git_string {
                hash.push('-');
                hash.push_str(git_branch.as_ref());
            }
        }
    }

    git_string
}

fn version_info() -> Result<String, anyhow::Error> {
    let cargo_toml = Path::new(&std::env::var("CARGO_MANIFEST_DIR")?).join("Cargo.toml");

    let mut file = File::open(&cargo_toml)?;

    let mut cargo_data = String::new();
    file.read_to_string(&mut cargo_data)?;

    let data: toml::Value = toml::from_str(&cargo_data)?;

    let name = data["package"]["name"]
        .as_str()
        .ok_or_else(|| anyhow::anyhow!("Missing package name string"))?;
    let version = data["package"]["version"]
        .as_str()
        .ok_or_else(|| anyhow::anyhow!("Missing version string"))?;

    Ok(format!("{}-{}", name, version))
}

fn main() -> Result<(), anyhow::Error> {
    memory_x();

    let version = version_info()?;
    let version = if let Some(git) = git_info() {
        format!("{}-{}", version, git)
    } else {
        version
    };

    println!("cargo:rustc-env=FIRMWARE_VERSION={}", version);

    Ok(())
}
