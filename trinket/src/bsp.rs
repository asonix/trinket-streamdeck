pub use atsamd_hal as hal;
pub use cortex_m_rt::entry;

use embedded_hal::timer::Periodic;
use hal::{
    gpio::v2::{
        Disabled, Floating, Input, Output, Pin, PullUp, PushPull, PA00, PA01, PA14, PA24, PA25,
    },
    prelude::_embedded_hal_timer_CountDown as CountDown,
    time::MegaHertz,
    usb::UsbBus,
};
use usb_device::class_prelude::UsbBusAllocator;

pub struct Dotstar {
    pub ci: Pin<PA01, Disabled<Floating>>,
    pub di: Pin<PA00, Disabled<Floating>>,
    pub nc: Pin<PA14, Disabled<Floating>>,
}

pub fn usb_allocator(
    usb: hal::pac::USB,
    clocks: &mut hal::clock::GenericClockController,
    pm: &mut hal::pac::PM,
    dm: Pin<PA24, Disabled<Floating>>,
    dp: Pin<PA25, Disabled<Floating>>,
) -> Option<UsbBusAllocator<UsbBus>> {
    let gckl0 = clocks.gclk0();
    let usb_clock = &clocks.usb(&gckl0)?;

    Some(UsbBusAllocator::new(UsbBus::new(
        usb_clock, pm, dm, dp, usb,
    )))
}

pub type Spi<C> = bitbang_hal::spi::SPI<
    Pin<PA14, Input<PullUp>>,
    Pin<PA00, Output<PushPull>>,
    Pin<PA01, Output<PushPull>>,
    C,
>;

impl Dotstar {
    pub fn init<C>(self, mut timer: C) -> apa102_spi::Apa102<Spi<C>>
    where
        C: CountDown + Periodic,
        C::Time: From<MegaHertz>,
    {
        // Adafruit Dotstar is always hardcoded to 8MHz
        timer.start(MegaHertz(8));

        let spi = bitbang_hal::spi::SPI::new(
            apa102_spi::MODE,
            self.nc.into(),
            self.di.into(),
            self.ci.into(),
            timer,
        );

        apa102_spi::Apa102::new_with_custom_postamble(spi, 4, false)
    }
}

hal::bsp_pins! (
    PA08 {
        name: d0
        aliases: {
            AlternateC: Sda
        }
    }
    PA02 {
        name: d1
        aliases: {
            AlternateC: Scl
        }
    }
    PA09 {
        name: d2
    }
    PA07 {
        name: d3
        aliases: {
            AlternateC: UartTx
        }
    }
    PA06 {
        name: d4
        aliases: {
            AlternateC: UartRx
        }
    }
    PA10 {
        name: d13
        aliases: {
            PushPullOutput: RedLed
        }
    }
    PA01 {
        name: dotstar_ci
        aliases: {
            AlternateD: Sclk
        }
    }
    PA00 {
        name: dotstar_di
        aliases: {
            AlternateD: Mosi
        }
    }
    PA14 {
        name: dotstar_nc
        aliases: {
            AlternateD: Miso
        }
    }
    PA31 {
        name: swdio
    }
    PA30 {
        name: swdclk
    }
    PA28 {
        name: usb_host_enable
    }
    PA23 {
        name: usb_sof
    }
    PA24 {
        name: usb_dm
        aliases: {
            AlternateG: UsbDm
        }
    }
    PA25 {
        name: usb_dp
        aliases: {
            AlternateG: UsbDp
        }
    }
);
