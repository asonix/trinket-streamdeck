use atsamd_hal::common::gpio::v2::{
    Disabled, Floating, Input, Pin, PullUp, PA02, PA07, PA08, PA09,
};
use common::{ButtonPins, ButtonState};
use core::convert::Infallible;
use embedded_hal::digital::v2::InputPin;

pub(crate) struct Buttons {
    pub(crate) d0: Pin<PA08, Disabled<Floating>>,
    pub(crate) d1: Pin<PA02, Disabled<Floating>>,
    pub(crate) d2: Pin<PA09, Disabled<Floating>>,
    pub(crate) d3: Pin<PA07, Disabled<Floating>>,
    // pub(crate) d4: Pin<PA06, Disabled<Floating>>,
}

pub(crate) struct Pins {
    d0: Pin<PA08, Input<PullUp>>,
    d1: Pin<PA02, Input<PullUp>>,
    d2: Pin<PA09, Input<PullUp>>,
    d3: Pin<PA07, Input<PullUp>>,
    // d4: Pin<PA06, Input<PullUp>>,
}

impl<'a> ButtonPins<'a, 4> for Pins {
    fn to_array(&'a self) -> [&'a dyn InputPin<Error = Infallible>; 4] {
        [&self.d0, &self.d1, &self.d2, &self.d3]
    }
}

impl Buttons {
    pub(crate) fn init(self) -> ButtonState<Pins, 4> {
        ButtonState::from_pins(Pins {
            d0: self.d0.into(),
            d1: self.d1.into(),
            d2: self.d2.into(),
            d3: self.d3.into(),
            // d4: self.d4.into(),
        })
    }
}
