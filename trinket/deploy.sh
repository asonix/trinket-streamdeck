#!/usr/bin/env bash

PROJECT=${1:-trinket-streamdeck}

function bossac() {
    # arduino ide flatpak
    $HOME/.arduino15/packages/arduino/tools/bossac/1.9.1-arduino2/bossac "${@:1}"

    # arduino ide snap
    # $HOME/snap/arduino/current/.arduino15/packages/arduino/tools/bossac/1.8.0-48-gb176eee/bossac "${@:1}"
}

set -e

cargo build --release -p $PROJECT
arm-none-eabi-objcopy -O binary \
    target/thumbv6m-none-eabi/release/$PROJECT \
    target/thumbv6m-none-eabi/release/$PROJECT.bin

ports="$(ls /dev/ | grep 'ttyACM')"

for port in $ports; do
    if [ "$port" == "" ]; then
        echo "Port missing"
        exit 1
    fi

    device="/dev/$port"

    printf "connecting to $device"
    while ! $(/bin/stty -F $device ospeed 1200 2> /dev/null); do
        printf "."
        sleep 1
    done
    echo

    bossac -i -d \
        --port "$port" -U -e -w -v -o 0x2000 \
        target/thumbv6m-none-eabi/release/$PROJECT.bin -R
done
